#include <iostream>
#include <string>

using namespace std;

class OnlineVid
 {
    private:  //properties
         int  views;
         string id;


    public:  //methods
        
         OnlineVid();//default constructor 
         OnlineVid(int, string);
			
          void setviews(int);
          int getviews();
          
          void setid(string);
          string getid();
          
 };
 
 

